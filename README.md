## Windsensor WiFi 1000 Documentation Server 

This page contains the HTTP server for the wind sensor online documentation. It is based on GitLab Pages and is started as a container. The HTTP server contains the following content:

* Version-dependent HTML documentation pages
* Directory of images
* Firmware directory
* Version directory

The online documentation can be found under the following links:

Online Documentation V1.01 https://norbertwalter67.gitlab.io/plain-html/index_V1.01.html

Online Documentation V1.03 https://norbertwalter67.gitlab.io/plain-html/index_V1.03.html

Online Documentation V1.04 https://norbertwalter67.gitlab.io/plain-html/index_V1.04.html

Online Documentation V1.05 https://norbertwalter67.gitlab.io/plain-html/index_V1.05.html

Online Documentation V1.06 https://norbertwalter67.gitlab.io/plain-html/index_V1.06.html

Online Documentation V1.07 https://norbertwalter67.gitlab.io/plain-html/index_V1.07.html

Online Documentation V1.08 https://norbertwalter67.gitlab.io/plain-html/index_V1.08.html

Online Documentation V1.09 https://norbertwalter67.gitlab.io/plain-html/index_V1.09.html

Online Documentation V1.10 https://norbertwalter67.gitlab.io/plain-html/index_V1.10.html

Online Documentation V1.11 https://norbertwalter67.gitlab.io/plain-html/index_V1.11.html

Online Documentation V1.12 https://norbertwalter67.gitlab.io/plain-html/index_V1.12.html

Online Documentation V1.13 https://norbertwalter67.gitlab.io/plain-html/index_V1.13.html

Online Documentation V1.14 https://norbertwalter67.gitlab.io/plain-html/index_V1.14.html

Online Documentation V1.15 https://norbertwalter67.gitlab.io/plain-html/index_V1.15.html

Online Documentation V1.16 https://norbertwalter67.gitlab.io/plain-html/index_V1.16.html

Online Documentation V1.17 https://norbertwalter67.gitlab.io/plain-html/index_V1.17.html





